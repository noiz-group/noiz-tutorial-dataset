# Noiz tutorial dataset

This is a seismic dataset that is prepared purposefully to guide users of Noiz in their first setps with the software

Data comes from a real deployment in mainland France.
For privacy reasons, names of network and stations as well as locations of devices were altered.
